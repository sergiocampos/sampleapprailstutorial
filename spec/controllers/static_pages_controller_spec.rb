require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do

  describe "GET #root" do
    it "renders the :home template" do
      expect(get: root_url(subdomain: nil)).to route_to(controller: "static_pages", action: "home")
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #home" do
    it "returns http success" do
      get :home
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #help" do
    it "returns http success" do
      get :help
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #about" do
    it "return http success" do
      get :about
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #contact" do
    it "return http success" do
      get :contact
      expect(response).to have_http_status(:success)
    end
  end
end
